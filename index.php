<?php

use App\Core\Request\Request;
use App\Core\Request\Status;
use App\Core\Router\Router;

require_once './vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->safeLoad();

echo Status::PENDING->value;


try {
    Router::loadWebRoutes()
        ->direct(Request::uri(), Request::method());
} catch (Throwable $th) {
    echo $th->getMessage();
}





